#!/bin/bash

DATE_ID=${1:-"20200109"}
CINEMA_LENGTH=$(curl -s -L https://feeds.drafthouse.com/adcService/showtimes.svc/market/0000 | jq --arg DATE_ID "$DATE_ID" '.Market.Dates[] | select(.DateId == $DATE_ID) | .Cinemas | length')

flash() {
  local title="LOTR CHECKER"
  local msg=$1
  osascript -e "display notification \"${msg}\" with title \"${title}\""
}

if [ -z "$CINEMA_LENGTH" ]; then
  msg="UH OH SOMETHINGS WRONG!"
  flash "${msg}"
  say ${msg}
  exit
fi

if [ "$CINEMA_LENGTH" -eq "1" ]; then
  msg="Nothing yet!"
  echo "${msg}"
  exit
fi

if [ "$CINEMA_LENGTH" -gt "1" ]; then
  msg="BUY BUY BUY!" 
  flash "${msg}"
  say ${msg}
  open https://drafthouse.com/austin/show/lord-of-the-rings-trilogy-feast
  exit
fi
